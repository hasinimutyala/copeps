﻿<!DOCTYPE html>
<html>
<head>
<title>SRKR Answers</title>
<link rel="stylesheet" href="index.css" media="all" type="text/css" />
<style>
body{
background-image:url("images/building.jpg");
background-repeate:none;
}
#error {
 border:1px solid #FDE0D0;
 background-color:#F4927B;
 color:#FFFFFF;
 margin:auto;
 max-width:300px;
 padding:5px;
 border-radius:2px;
}
#total {
  margin:auto;
  text-align:center;
}
#formdiv {
 border:1px solid #A6C2DC;
 background-color:#ECF5FE;
 margin:0 auto;
 max-width:400px;
 margin-top:100px;
 margin-bottom:10px;
 padding:40px;
 padding-top:10px;
 border-radius:2px;
}
#logint{
margin:0px;
font-size:25px;
}
#logints{
margin:0px;
}
input{
  background-color:#FFFFFF;
  border:1px solid #3B7DB9;
  border-radius:2px;
  width:100%;
   height:27px;
  border-radius:3px;
  background-color:#FFFFFF;
}
#submit {
  background-color:#3B7DB9;
  border:1px solid #3B7DB9;
  border-radius:2px;
  width:80px;
  height:26px;
  color:#FFFFFF;
  margin:10px;
}
</style>
</head>
<body>
<div id="total">
	<div id="formdiv">
<a href="index.php" title="Go to home"><img src="images/logo.png" id="logo"></a>

<?php require_once('dbconnect.php'); ?>

<?php
	  $dbc = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
	  if (isset($_POST['submit'])) 
	  {
	     $regno = mysqli_real_escape_string($dbc, trim($_POST['regno']));
	     $pass = mysqli_real_escape_string($dbc, trim($_POST['pass']));
		if(!empty($regno) && !empty($pass))
		{
		         $query = "SELECT userid, regno, status FROM user WHERE regno = '$regno' AND pass = SHA('$pass')";
       			 $data = mysqli_query($dbc, $query);
				if (mysqli_num_rows($data) == 1) 
				{
				 $row = mysqli_fetch_array($data);
				 $_SESSION['userid'] = $row['userid'];
				 $_SESSION['regno'] = $row['regno'];
				 setcookie('userid', $row['userid'], time() + (60 * 60 * 24 * 30));
				 setcookie('regno', $row['regno'], time() + (60 * 60 * 24 * 30));

			if($row['status'] == 'principal')
			         $home_url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/principal/home.php';
			else if($row['status'] == 'student')
			         $home_url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/student/home.php';
			else if($row['status'] == 'HOD')
			         $home_url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/hod/home.php';
			else
			         $home_url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/staff/home.php';
			         header('Location: ' . $home_url);
	  		        }
	  			 else
				    {
				      echo '<p id="error">Please give correct login details or contact Admin in case of any problem</p>';	
				    }
		}
		 else
		    {
		      echo '<p id="error">Please give login details</p>';	
		    }
	 }
?>


	 <p id="logint">LOGIN</p>
	 <p id="logints">Please Enter your login details</p>
       	 <form enctype="multipart/form-data" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
     	 <label for="regno" id="regnol">Login Id:</label><br>
      	 <input type="text" name="regno" id="regno" value="<?php if (!empty($regno)) echo $regno; ?>" ><br />
      	 <label for="pass" id="passl">Password:</label><br>
      	 <input type="password" name="pass" id="pass" ><br>
         <input type="submit" value="Submit" name="submit" id="submit" />
	 </form>

	</div>
</div>
<?php require_once('footer.php'); ?>
</body>

</html>