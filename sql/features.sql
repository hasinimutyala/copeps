﻿CREATE TABLE `features` (
  `fid` INT AUTO_INCREMENT,
  `pid` VARCHAR(100),
  `descr` LONGTEXT,
  PRIMARY KEY (`fid`)
);