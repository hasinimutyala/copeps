﻿CREATE TABLE `b_profile` (
  `bid` INT AUTO_INCREMENT,
  `uid` VARCHAR(100),
  `burl` VARCHAR(100),
  `bname` VARCHAR(100),
  `btype` VARCHAR(100),
  `badd` VARCHAR(100),
  `bcity` VARCHAR(100),
  `bdist` VARCHAR(100),
  `bpin` VARCHAR(100),
  `bemail` VARCHAR(100),
  `bphone` VARCHAR(100),
  `bstatus` VARCHAR(100),
  `btimef` VARCHAR(100),
  `btimet` VARCHAR(100),
  PRIMARY KEY (`bid`)
);