﻿CREATE TABLE `products` (
  `pid` INT AUTO_INCREMENT,
  `uid` VARCHAR(100),
  `pname` VARCHAR(100),
  `price` VARCHAR(100),
  `discount` VARCHAR(100),
  `imag` LONGTEXT,
  `stock` VARCHAR(100),
  `sold` VARCHAR(100),
  `descr` LONGTEXT,
  `joindate` DATETIME,
  PRIMARY KEY (`pid`)
);