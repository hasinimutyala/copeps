﻿CREATE TABLE `orders` (
  `oid` INT AUTO_INCREMENT,
  `pid` VARCHAR(100),
  `uid` VARCHAR(100),
  `rtype` VARCHAR(100),
  `rdate` DATETIME,
  `ostatus` VARCHAR(100),
  PRIMARY KEY (`oid`)
);