﻿CREATE TABLE `buser` (
  `uid` INT AUTO_INCREMENT,
  `phone` VARCHAR(100),
  `password` VARCHAR(100),
  `burl` VARCHAR(100),
  `bname` VARCHAR(100),
  `btype` VARCHAR(100),
  `badd` VARCHAR(100),
  `bcity` VARCHAR(100),
  `bdist` VARCHAR(100),
  `bpin` VARCHAR(100),
  `bemail` VARCHAR(100),
  `bphone` VARCHAR(100),
  `bstatus` VARCHAR(100),
  `joindate` DATETIME,
  PRIMARY KEY (`uid`)
);