﻿CREATE TABLE `nuser` (
  `uid` INT AUTO_INCREMENT,
  `phone` VARCHAR(100),
  `password` VARCHAR(100),
  `uname` VARCHAR(100),
  `uadd` VARCHAR(100),
  `ucity` VARCHAR(100),
  `udist` VARCHAR(100),
  `upin` VARCHAR(100),
  `uemail` VARCHAR(100),
  `uphone` VARCHAR(100),
  PRIMARY KEY (`uid`)
);