﻿<?php require_once('access.php'); ?>
<?php require_once('header.php'); ?>
<?php require_once('nav.php'); ?>
<?php
  if (isset($_POST['submit'])) {
	$burl = mysqli_real_escape_string($dbc, trim($_POST['burl']));
	$bname = mysqli_real_escape_string($dbc, trim($_POST['bname']));
	$btype = mysqli_real_escape_string($dbc, trim($_POST['btype']));
	$badd = mysqli_real_escape_string($dbc, trim($_POST['badd']));
	$bcity  = mysqli_real_escape_string($dbc, trim($_POST['bcity']));
	$bdist = mysqli_real_escape_string($dbc, trim($_POST['bdist']));
	$bpin = mysqli_real_escape_string($dbc, trim($_POST['bpin']));
	$bemail = mysqli_real_escape_string($dbc, trim($_POST['bemail']));
	$bphone = mysqli_real_escape_string($dbc, trim($_POST['bphone']));
  if (isset($_POST['bstatus']))
	$bstatus = mysqli_real_escape_string($dbc, trim($_POST['bstatus']));
  else
	$bstatus = 'n';

if(!empty($burl) && !empty($bname) && !empty($btype) && !empty($badd) && !empty($bcity) && !empty($bdist) && !empty($bpin) && !empty($bphone)) {
	     $query = "SELECT * FROM buser WHERE burl = '$burl' AND uid != '" . $_SESSION['uid'] . "'";
	      $data = mysqli_query($dbc, $query);
    	       if (mysqli_num_rows($data) == 0) {
		$query = "UPDATE buser SET burl = '$burl', bname = '$bname', btype = '$btype', badd = '$badd', bcity = '$bcity', bdist = '$bdist', bpin = '$bpin', bemail = '$bemail', bphone = '$bphone', bstatus = '$bstatus' WHERE uid = '" . $_SESSION['uid'] . "'";
		      mysqli_query($dbc, $query);

          $home_url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/home.php';
          header('Location: ' . $home_url);

		}
	else {
		echo 'This url is already taken';
	}
	}
	else {
		echo 'Fill all mandatory details';
	}

}
?>


Please Give your details
   <form enctype="multipart/form-data" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">

      <label for="burl" id="burll">Your Business url:</label><br>
      <input type="text" name="burl" id="burl" value="<?php if (!empty($burl)) echo $burl; ?>" ><br>

      <label for="bname" id="bnamel">Your Business Name:</label><br>
      <input type="text" name="bname" id="bname" value="<?php if (!empty($bname)) echo $bname; ?>" ><br>


  <label for="btype" id ="btypel">Business Type:</label><br>
  <select name="btype" id="btype">
  <option>Select</option>
  <option value="Fancy" <?php if (!empty($btype) && $btype == 'Fancy') echo 'selected = "selected"'; ?>>Fancy Store</option>        
  <option value="Kirana and General" <?php if (!empty($btype) && $btype == 'Kirana and General') echo 'selected = "selected"'; ?>>Kirana and General Store</option>        
  <option value="Fruits" <?php if (!empty($btype) && $btype == 'Fruits') echo 'selected = "selected"'; ?>>Fruits Store</option>        
  <option value="Vegies" <?php if (!empty($btype) && $btype == 'Vegies') echo 'selected = "selected"'; ?>>Vegies Store</option>        
  <option value="Hardware" <?php if (!empty($btype) && $btype == 'Hardware') echo 'selected = "selected"'; ?>>Hardware Store</option>
  <option value="Agriculture" <?php if (!empty($btype) && $btype == 'Agriculture') echo 'selected = "selected"'; ?>>Agriculture Products Store</option>        
  <option value="Vehicals" <?php if (!empty($btype) && $btype == 'Vehicals') echo 'selected = "selected"'; ?>>Vehicals Store</option>        
  <option value="Footwear" <?php if (!empty($btype) && $btype == 'Footwear') echo 'selected = "selected"'; ?>>Footwear Store</option>        
  <option value="Cloth" <?php if (!empty($btype) && $btype == 'Cloth') echo 'selected = "selected"'; ?>>Cloth Store</option>        
  <option value="Food" <?php if (!empty($btype) && $btype == 'Food') echo 'selected = "selected"'; ?>>Food Store</option>        
  <option value="Jewalry" <?php if (!empty($btype) && $btype == 'Jewalry') echo 'selected = "selected"'; ?>>Jewalry Store</option>        
  <option value="Medical" <?php if (!empty($btype) && $btype == 'Medical') echo 'selected = "selected"'; ?>>Medical Store</option>        
  <option value="Bakery" <?php if (!empty($btype) && $btype == 'Bakery') echo 'selected = "selected"'; ?>>Bakery Store</option>        
  <option value="Curry" <?php if (!empty($btype) && $btype == 'Curry') echo 'selected = "selected"'; ?>>Curry Point</option>        
  </select><br>

  <label for="badd" id ="baddl">Store's Address:</label><br>
  <textarea id="badd" name="badd"></textarea><br>

      <label for="bcity" id="bcityl">City:</label><br>
      <input type="text" name="bcity" id="bcity" value="<?php if (!empty($bcity)) echo $bcity; ?>" ><br>

      <label for="bdist" id="bdistl">District:</label><br>
      <input type="text" name="bdist" id="bdist" value="<?php if (!empty($bdist)) echo $bdist; ?>" ><br>

      <label for="bpin" id="bpinl">PIN CODE:</label><br>
      <input type="text" name="bpin" id="bpin" value="<?php if (!empty($bpin)) echo $bpin; ?>" ><br>



      <label for="bemail" id="bemaill">Business Email Address:</label><br>
      <input type="text" name="bemail" id="bemail" value="<?php if (!empty($bemail)) echo $bemail; ?>" ><br>

      <label for="bphone" id="bphonel">Business Phone Number:</label><br>
      <input type="text" name="bphone" id="bphone" value="<?php if (!empty($bphone)) echo $bphone; ?>" ><br>

      <label for="bstatus" id="bstatusl">Home Delivery:</label>
      <input type="checkbox" name="bstatus" id="bstatus" value="y"><br>


      <input type="submit" value="UPDATE" name="submit" id="submit" />
   </form>

<?php require_once('footer.php'); ?>
