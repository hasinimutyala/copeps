﻿<?php require_once('access.php'); ?>
<?php require_once('header.php'); ?>


<?php
  if (isset($_POST['submit'])) {
	$burl = mysqli_real_escape_string($dbc, trim($_POST['burl']));
	$bname = mysqli_real_escape_string($dbc, trim($_POST['bname']));
	$btype = mysqli_real_escape_string($dbc, trim($_POST['btype']));
	$badd = mysqli_real_escape_string($dbc, trim($_POST['badd']));
	$bcity  = mysqli_real_escape_string($dbc, trim($_POST['bcity']));
	$bdist = mysqli_real_escape_string($dbc, trim($_POST['bdist']));
	$bpin = mysqli_real_escape_string($dbc, trim($_POST['bpin']));
	$bemail = mysqli_real_escape_string($dbc, trim($_POST['bemail']));
	$bphone = mysqli_real_escape_string($dbc, trim($_POST['bphone']));
	$bstatus = mysqli_real_escape_string($dbc, trim($_POST['bstatus']));
	$btimef = mysqli_real_escape_string($dbc, trim($_POST['btimef']));
	$btimet = mysqli_real_escape_string($dbc, trim($_POST['btimet']));
	
}
?>


Please Give your details
   <form enctype="multipart/form-data" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">

      <label for="burl" id="burll">Your Business url:</label><br>
      <input type="text" name="burl" id="burl" value="<?php if (!empty($burl)) echo $burl; ?>" ><br>

      <label for="bname" id="bnamel">Your Business Name:</label><br>
      <input type="text" name="bname" id="bname" value="<?php if (!empty($bname)) echo $bname; ?>" ><br>


  <label for="btype" id ="btypel">Business Type:</label><br>
  <select name="btype" id="btype">
  <option>Select</option>
  <option value="Fancy" <?php if (!empty($btype) && $btype == 'Fancy') echo 'selected = "selected"'; ?>>Fancy Store</option>        
  <option value="Kirana and General" <?php if (!empty($btype) && $btype == 'Kirana and General') echo 'selected = "selected"'; ?>>Kirana and General Store</option>        
  <option value="Fruits" <?php if (!empty($btype) && $btype == 'Fruits') echo 'selected = "selected"'; ?>>Fruits Store</option>        
  <option value="Vegies" <?php if (!empty($btype) && $btype == 'Vegies') echo 'selected = "selected"'; ?>>Vegies Store</option>        
  <option value="Hardware" <?php if (!empty($btype) && $btype == 'Hardware') echo 'selected = "selected"'; ?>>Hardware Store</option>
  <option value="Agriculture" <?php if (!empty($btype) && $btype == 'Agriculture') echo 'selected = "selected"'; ?>>Agriculture Products Store</option>        
  <option value="Vehicals" <?php if (!empty($btype) && $btype == 'Vehicals') echo 'selected = "selected"'; ?>>Vehicals Store</option>        
  <option value="Footwear" <?php if (!empty($btype) && $btype == 'Footwear') echo 'selected = "selected"'; ?>>Footwear Store</option>        
  <option value="Cloth" <?php if (!empty($btype) && $btype == 'Cloth') echo 'selected = "selected"'; ?>>Cloth Store</option>        
  <option value="Food" <?php if (!empty($btype) && $btype == 'Food') echo 'selected = "selected"'; ?>>Food Store</option>        
  <option value="Jewalry" <?php if (!empty($btype) && $btype == 'Jewalry') echo 'selected = "selected"'; ?>>Jewalry Store</option>        
  <option value="Medical" <?php if (!empty($btype) && $btype == 'Medical') echo 'selected = "selected"'; ?>>Medical Store</option>        
  <option value="Bakery" <?php if (!empty($btype) && $btype == 'Bakery') echo 'selected = "selected"'; ?>>Bakery Store</option>        
  <option value="Curry" <?php if (!empty($btype) && $btype == 'Curry') echo 'selected = "selected"'; ?>>Curry Point</option>        
  </select><br>

  <label for="badd" id ="baddl">Store's Address:</label><br>
  <textarea id="badd" name="badd"></textarea><br>

      <label for="bcity" id="bcityl">City:</label><br>
      <input type="text" name="bcity" id="bcity" value="<?php if (!empty($bcity)) echo $bcity; ?>" ><br>

      <label for="bdist" id="bdistl">District:</label><br>
      <input type="text" name="bdist" id="bdist" value="<?php if (!empty($bdist)) echo $bdist; ?>" ><br>

      <label for="bpin" id="bpinl">PIN CODE:</label><br>
      <input type="text" name="bpin" id="bpin" value="<?php if (!empty($bpin)) echo $bpin; ?>" ><br>


      <label for="btimef" id="btimefl">Business Time From:</label><br>
      <input type="text" name="btimef" id="btimef" value="<?php if (!empty($btimef)) echo $btimef; ?>" ><br>

      <label for="btimet" id="btimetl">Business Time To:</label><br>
      <input type="text" name="btimet" id="btimet" value="<?php if (!empty($btimet)) echo $btimet; ?>" ><br>

      <input type="submit" value="UPDATE" name="submit" id="submit" />
   </form>

<?php require_once('footer.php'); ?>
