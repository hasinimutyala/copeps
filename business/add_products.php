﻿<?php require_once('access.php'); ?>
<?php require_once('header.php'); ?>
<?php require_once('nav.php'); ?>
<?php require_once('dbconnect.php'); ?>

<?php
  if (isset($_POST['submit'])) {
	$pname = mysqli_real_escape_string($dbc, trim($_POST['pname']));
	$price = mysqli_real_escape_string($dbc, trim($_POST['price']));
	$discount = mysqli_real_escape_string($dbc, trim($_POST['discount']));
	$stock = mysqli_real_escape_string($dbc, trim($_POST['stock']));
	$sold = mysqli_real_escape_string($dbc, trim($_POST['sold']));
	$descr = mysqli_real_escape_string($dbc, trim($_POST['descr']));

        $imag = mysqli_real_escape_string($dbc, trim($_FILES['imag']['name']));
        $imag_type = $_FILES['imag']['type'];
        $imag_size = $_FILES['imag']['size']; 
        list($imag_width, $imag_height) = getimagesize($_FILES['imag']['tmp_name']);

	     $query = "SELECT max(pid) FROM products";
	     $data = mysqli_query($dbc, $query);

		 $row = mysqli_fetch_array($data);
if(is_numeric($row[0]))
		 $imagname = md5(uniqid(uniqid($row[0]+1)));
		else
		$imagname = md5(uniqid(uniqid(1)));

if(!empty($pname)) {
if(!empty($imag)) {
   if ((($imag_type == 'image/gif') || ($imag_type == 'image/jpeg') || ($imag_type == 'image/pjpeg') ||
        ($imag_type == 'image/png')) && ($imag_size > 0) && ($imag_size <= 999999999) &&
        ($imag_width <= 1000) && ($imag_height <= 1000)) {
      if ($_FILES['imag']['error'] == 0) {
	  $imagname = $imagname . basename($imag);
        $target = 'pimags/'.$imagname;
  	  if(move_uploaded_file($_FILES['imag']['tmp_name'], $target))
	  {
	      $query = "INSERT INTO products (uid, pname, price, discount, imag, stock, sold, descr, joindate) VALUES ('" . $_SESSION['uid'] . "', '$pname', '$price', '$discount', '$imagname', '$stock', '$sold', '$descr', NOW())";
	      mysqli_query($dbc, $query);
	echo '<br>'.$pname.' is added sucessfully';
	$pname='';
	$price='';
	$discount='';
	$stock='';
	$sold='';
	$descr='';
	$imag='';
	  }
      }
	else {
            @unlink($_FILES['imag']['tmp_name']);
	    echo 'Sorry there was a problem uploading image';
	}
    }
	else {
            @unlink($_FILES['imag']['tmp_name']);
	 echo 'image type and size error';
	}
}
else{
      $query = "INSERT INTO products (uid, pname, price, discount, imag, stock, sold, descr, joindate) VALUES ('" . $_SESSION['uid'] . "', '$pname', '$price', '$discount', 'n', '$stock', '$sold', '$descr', NOW())";
      mysqli_query($dbc, $query);
	echo '<br>'.$pname.' is added sucessfully';
	$pname='';
	$price='';
	$discount='';
	$stock='';
	$sold='';
	$descr='';
	$imag='';
}
}
else {
      echo 'Please Enter Product Name';
     }

}

?>

<br>Please enter Product details
   <form enctype="multipart/form-data" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">

      <label for="pname" id="pnamel">Product Name:</label><br>
      <input type="text" name="pname" id="pname" value="<?php if (!empty($pname)) echo $pname; ?>" ><br>

      <label for="price" id="pricel">Price:</label><br>
      <input type="text" name="price" id="price" value="<?php if (!empty($price)) echo $price; ?>" ><br>

      <label for="discount" id="discountl">Discount:</label><br>
      <input type="text" name="discount" id="discount" value="<?php if (!empty($discount)) echo $discount; ?>" ><br>

      <label for="stock" id="stockl">Stock Remaining:</label><br>
      <input type="text" name="stock" id="stock" value="<?php if (!empty($stock)) echo $stock; ?>" ><br>

      <label for="sold" id="soldl">Sold items:</label><br>
      <input type="text" name="sold" id="sold" value="<?php if (!empty($sold)) echo $sold; ?>" ><br>


      <label for="descr" id="descrl">Description:</label><br>
      <textarea id="descr" name="descr"></textarea><br>

      <label for="imag" id="imagl">Product Image:</label><br>
      <input type="file" name="imag" id="imag" ><br>


      <input type="submit" value="Add Product" name="submit" id="submit" />
   </form>

<?php require_once('footer.php'); ?>