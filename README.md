# Copeps

  Copeps is an e-commerce software that helps buy, sell and exchange products.

## Install

```
  # clone the project into your server
  git clone https://hasinimutyala@bitbucket.org/hasinimutyala/copeps.git
  # Modify Database connection details in "dbconnect.php" file
  # Create a database "copeps" and import all .sql files from 'sql' directory
```
